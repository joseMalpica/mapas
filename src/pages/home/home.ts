import { Component} from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  map: GoogleMap;
  myPosition: any = {};
  name : any;
  markers: any[] = [
    {
      position:{
        latitude: 19.631371,
        longitude: -99.300976,
      },
      title:'PUC casa mama'
      //icon: 'www/assets/imgs/marker-green.png'
    },
    {
      position:{
        latitude: 19.617525,
        longitude: -99.334441,
      },
      title:'PUC trabajo'
      //icon: 'www/assets/imgs/marker-green.png'
    },
    {
      position:{
        latitude: 19.624530,
        longitude: -99.336278,
      },
      title:'PUC untitle'
      //icon: 'www/assets/imgs/marker-green.png'
    },
  ];

  constructor(
    private navCtrl: NavController,
    private geolocation: Geolocation,
    private googleMaps: GoogleMaps
  ) {
    this.name = "PUC puksPapu";
   console.log(this.name);
  }

  ionViewDidLoad(){
    this.getCurrentPosition();
  }

  getCurrentPosition(){
    this.geolocation.getCurrentPosition()
    .then(position => {
      this.myPosition = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }
      this.loadMap();
    })
    .catch(error=>{
      console.log(error);
    })
  }

  loadMap(){
    // create a new map by passing HTMLElement
    let element: HTMLElement = document.getElementById('map');

    this.map = this.googleMaps.create(element);

    // create CameraPosition
    let position: CameraPosition = {
      target: new LatLng(this.myPosition.latitude, this.myPosition.longitude),
      zoom: 12,
      tilt: 30
    };

    this.map.one(GoogleMapsEvent.MAP_READY).then(()=>{
      console.log('Map is ready!');

      // move the map's camera to position
      this.map.moveCamera(position);

      let markerOptions: MarkerOptions = {
        position: this.myPosition,
        title: "Hello",
      };

      this.addMarker(markerOptions);

      this.markers.forEach(marker=>{
        this.addMarker(marker);
      });

    });
  }

  addMarker(options){
    let markerOptions: MarkerOptions = {
      position: new LatLng(options.position.latitude, options.position.longitude),
      title: options.title,
      icon: options.icon,
    };
    this.map.addMarker(markerOptions).then((marker: Marker) => {
             marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
            .subscribe(e => {
              //alert(e.get('title'));
              this.name=e.get('title');
              //let rec = data1.filter(v=>v.title==e.get('title'));
              //alert(rec[0].id);
            });
    });
  }


}
